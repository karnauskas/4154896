class Foo
  property :id, Seriaal
  property :name, STring
  has n, :bar

  # Enumerate bar with name
  #
  # @return [self]
  #   if block given
  #
  # @return [Enumerator<Array>]
  #   otherwise
  #
  # @api private
  #
  def bar_with_name(query={})
    return to_enum(__method__) unless block_given?

    bar.all(query).map do |bar|
      yield [bar, name]
    end

    self
  end
end

class Bar
  belongs_to :foo
  property :something
end

Foo.first.bar_with_name(:your => :restriction).each do |bar, name|
  # do your stuff
end
